# 代码评审人推荐工具

#### 介绍
通过订阅本系统的服务，可以根据历史评审信息给每个提交的Pull Request推荐对应的评审人来进行评审。


#### 使用说明

1. 首先订阅服务。将需要使用本服务的项目的主页地址发送到邮箱：pingyyuan@buaa.edu.cn。
2. 在需要部署服务的项目里部署webhook。

* 2.1在项目主页选择“管理”栏。

![管理栏](https://images.gitee.com/uploads/images/2021/0731/210008_369cc919_8493309.png "管理栏箭头.png")

* 2.2点击左边的webhook，并添加一个新的webhook。

![找到webhook](https://images.gitee.com/uploads/images/2021/0731/210049_3e4fb354_8493309.png "找到webhook箭头.png")

* 2.3在新建的webhook里，url地址填写http://117.50.99.180:80。然后在触发事件里勾选Pull Request。最后点击添加即可绑定评审人推荐服务。

![添加webhook](https://images.gitee.com/uploads/images/2021/0731/210206_d128f4ac_8493309.png "添加webhhok箭头.png")

* 2.4展示结果如下：

![展示结果](https://images.gitee.com/uploads/images/2021/0731/223047_d393c58d_8493309.png "推荐结果4.png")

